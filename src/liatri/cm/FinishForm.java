package liatri.cm;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FinishForm {
    private JPanel panel1;
    private JLabel result;
    private JLabel trad1;
    private JLabel trad2;
    private JLabel trad3;
    private JLabel trad4;
    private JLabel trad5;
    private JLabel radiant;
    private JButton Exit;
    private JButton onesmore;
    private JLabel tdire1;
    private JLabel tdire2;
    private JLabel tdire3;
    private JLabel tdire4;
    private JLabel tdire5;
    private JPanel commands;
    private JPanel res;
    JFrame finishFrame;


    public FinishForm(){
        finishFrame = new JFrame();
        finishFrame.setBounds(0,0,20, 20);
        Hero[] pick = StartGame.scr.getPicked();
        trad1.setIcon(pick[0].getIcon());
        trad2.setIcon(pick[1].getIcon());
        trad3.setIcon(pick[2].getIcon());
        trad4.setIcon(pick[3].getIcon());
        trad5.setIcon(pick[4].getIcon());
        tdire1.setIcon(pick[5].getIcon());
        tdire2.setIcon(pick[6].getIcon());
        tdire3.setIcon(pick[7].getIcon());
        tdire4.setIcon(pick[8].getIcon());
        tdire5.setIcon(pick[9].getIcon());

        Exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        onesmore.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                finishFrame.dispose();
                StartGame.scr.createNewGame();
            }
        });
        finishFrame.add(panel1);
        finishFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        finishFrame.setSize(new Dimension(460, 350));
        finishFrame.setVisible(true);
        finishFrame.setLocationRelativeTo(null);
    }


}
