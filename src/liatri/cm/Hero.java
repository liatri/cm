package liatri.cm;

import javax.swing.*;

public class Hero {
    private String name;
    private ImageIcon icon;
    private ImageIcon iconChoose;
    private ImageIcon iconBanned;
    private int number;
    private static int numberOfChosenHero;


    public Hero(String name, ImageIcon icon) {
        this.name = name;
        this.icon = icon;
    }

    public ImageIcon getIconChoose() {
        return iconChoose;
    }

    public void setIconChoose(ImageIcon iconChoose) {
        this.iconChoose = iconChoose;
    }

    public ImageIcon getIconBanned() {
        return iconBanned;
    }

    public void setIconBanned(ImageIcon iconBanned) {
        this.iconBanned = iconBanned;
    }

    public Hero() {
        this.name = "noname";
        this.icon = new ImageIcon("src/liatri/cm/1.png");
        this.iconChoose = new ImageIcon("src/liatri/cm/2.png");
        this.iconBanned = new ImageIcon("src/liatri/cm/2.png");
        this.number = 0;
    }

    public ImageIcon getIcon() {
        return icon;

    }

    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public static int getNumberOfChosenHero() {
        return numberOfChosenHero;
    }

    public static void setNumberOfChosenHero(int numberOfChosenHero) {
        Hero.numberOfChosenHero = numberOfChosenHero;
    }

    @Override
    public String toString() {
        return  name + "";
    }
}
