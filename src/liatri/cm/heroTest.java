package liatri.cm;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.net.URL;

public class heroTest {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel mainPanel = new JPanel();
        JLabel av1 = new JLabel("av1");
        JLabel name1 = new JLabel("");
        JLabel av2 = new JLabel("av2");
        JLabel name2 = new JLabel("");
        Hero[] heroes = new Hero[2];
        try{
            JFileChooser fileOpen = new JFileChooser();
            fileOpen.showOpenDialog(frame);
            BufferedReader reader = new BufferedReader(new FileReader(fileOpen.getSelectedFile()));
            String line = null;
            int i = 0;
            while ((line = reader.readLine()) != null){
                String[] result = line.split("/");
                System.out.println(result[0] + " " + result[1]);
                heroes[i] = new Hero();
                heroes[i].setName(result[0]);
                heroes[i].setIcon(createIcon(result[1]));
                i++;
            }
        } catch (Exception e){e.printStackTrace();}
        av1.setIcon(heroes[0].getIcon());
        av2.setIcon(heroes[1].getIcon());
        name1.setText(heroes[0].getName());
        name2.setText(heroes[1].getName());
        mainPanel.add(name1);
        mainPanel.add(av1);
        mainPanel.add(name2);
        mainPanel.add(av2);
        frame.getContentPane().add(mainPanel);
        frame.setVisible(true);
    }
    protected static ImageIcon createIcon(String path) {
        URL imgURL = heroTest.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("File not found " + path);
            return null;
        }
    }
}
