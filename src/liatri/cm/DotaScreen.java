package liatri.cm;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashSet;


public class DotaScreen {
    private static int numberOfStep;
    private JFrame frame;
    private JPanel mainPanel;
    private JPanel pickedHeroes;
    private JLabel rad1;
    private JLabel rad2;
    private JLabel rad3;
    private JLabel rad4;
    private JLabel rad5;
    private JLabel rad6;
    private JLabel rad7;
    private JLabel rad8;
    private JLabel rad9;
    private JLabel rad10;
    private JPanel allHeroes;
    private JPanel pick;
    private JPanel strengthHeroes;
    private JPanel agilytyHeroes;
    private JPanel intelligenceHeroes;
    private JPanel pickPanel;
    private JButton CHOOSEButton;
    private JProgressBar progressBar1;
    private Hero[] heroes = new Hero[113];
    private JLabel[] pickLabels = new JLabel[20];
    private Hero[] picked = new Hero[10];
    private HeroButton[] listOfHeroes = new HeroButton[113];
    private boolean pickable;
    private static final HashSet<Integer> picks = new HashSet<Integer>() {{add(5); add(6); add(7); add(8); add(13);add(14);add(15);add(16);add(19);add(20);}};
    private static final HashSet<Integer> radiantBan = new HashSet<Integer>() {{add(1); add(3); add(10); add(12); add(18);}};
    private static final HashSet<Integer> direBan = new HashSet<Integer>() {{add(2); add(4); add(9); add(11); add(17);}};
    private static final HashSet<Integer> direPick = new HashSet<Integer>() {{add(5); add(8); add(14); add(16); add(19);}};


    public Hero[] getPicked() {
        return picked;
    }
    public DotaScreen() {
        numberOfStep = 1;
        pickable = false;
        frame = new JFrame();
        frame.setBounds(0,0,1200, 700);
        createMenu();
        createPickBanArea();
        heroes = fillHeroes();
        strengthHeroes.setLayout(new GridLayout(4, 11));
        intelligenceHeroes.setLayout(new GridLayout(4, 11));
        agilytyHeroes.setLayout(new GridLayout(4, 11));
        createAllHeroesArea();
        mainPanel.add(BorderLayout.SOUTH, new JLabel("123"));
        frame.add(mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        JOptionPane.showMessageDialog( frame, "Radiant team ban.");
    }
    public void createAllHeroesArea(){

        for (int i = 0; i < 44; i++) {

            if (i<37){
                listOfHeroes[i] = new HeroButton();
                Font BigFontTR = new Font("TimesRoman", Font.PLAIN, 1);
                listOfHeroes[i].setFont(BigFontTR);
                listOfHeroes[i].setPos(i);
                listOfHeroes[i].setIcon(heroes[i].getIcon());
                listOfHeroes[i].setBorderPainted(false);
                listOfHeroes[i].setContentAreaFilled(false);
                listOfHeroes[i].setMargin(new Insets(0, 0, 0, 0));
                listOfHeroes[i].addActionListener(new AllHeroesListener());
                listOfHeroes[i].setEnabled(true);
                strengthHeroes.add(listOfHeroes[i]);
            } else strengthHeroes.add(new JLabel(""));

        }

        for (int i = 0; i < 44; i++) {
            if (i<40){
                listOfHeroes[i+73] = new HeroButton();
                Font BigFontTR = new Font("TimesRoman", Font.PLAIN, 1);
                listOfHeroes[i+73].setPos(i+73);
                listOfHeroes[i+73].setFont(BigFontTR);
                listOfHeroes[i+73].setIcon(heroes[i+73].getIcon());
                listOfHeroes[i+73].setBorderPainted(false);
                listOfHeroes[i+73].setContentAreaFilled(false);
                listOfHeroes[i+73].setMargin(new Insets(0, 0, 0, 0));
                listOfHeroes[i+73].addActionListener(new AllHeroesListener());
                listOfHeroes[i+73].setEnabled(true);
                intelligenceHeroes.add(listOfHeroes[i+73]);
            } else intelligenceHeroes.add(new JLabel(""));

        }

        for (int i = 0; i < 44; i++) {

            if (i<36) {
                listOfHeroes[i+37] = new HeroButton();
                Font BigFontTR = new Font("TimesRoman", Font.PLAIN, 1);
                listOfHeroes[i+37].setPos(i+37);
                listOfHeroes[i+37].setFont(BigFontTR);
                listOfHeroes[i+37].setIcon(heroes[i+37].getIcon());
                listOfHeroes[i+37].setBorderPainted(false);
                listOfHeroes[i+37].setContentAreaFilled(false);
                listOfHeroes[i+37].setMargin(new Insets(0, 0, 0, 0));
                listOfHeroes[i+37].addActionListener(new AllHeroesListener());
                listOfHeroes[i+37].setEnabled(true);
                agilytyHeroes.add(listOfHeroes[i+37]);
            } else agilytyHeroes.add(new JLabel(""));

        }
    }
    public Hero[] fillHeroes() {
        Hero[] result = new Hero[113];
        try{
            File heroList = new File("src/liatri/cm/heroList.txt");
            BufferedReader reader = new BufferedReader(new FileReader(heroList));
            String line = null;
            int i = 0;
            while ((line = reader.readLine()) != null){
                String[] res = line.split("/");
                result[i] = new Hero();
                result[i].setName(res[0]);
                result[i].setIcon(new ImageIcon("src/liatri/cm/icons/"+res[1]));
                result[i].setIconChoose(new ImageIcon("src/liatri/cm/picked/"+res[1]));
                result[i].setIconBanned(new ImageIcon("src/liatri/cm/banned/"+res[1]));
                i++;
            }
        } catch (FileNotFoundException e) {
            System.out.println("cannot open file");
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return result;
    }
    public class AllHeroesListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            pickable = true;
            HeroButton btn = (HeroButton) e.getSource();
            int num = btn.getPos();
            Hero.setNumberOfChosenHero(num);
            ImageIcon settedIcon;
            if (picks.contains(numberOfStep)){
                settedIcon = heroes[num].getIconChoose();
            } else{
                settedIcon = heroes[num].getIconBanned();
            }
            pickLabels[numberOfStep-1].setIcon(settedIcon);
            switch (numberOfStep){
                case 5:rad6.setIcon(heroes[num].getIcon());
                    break;
                case 6:rad1.setIcon(heroes[num].getIcon());
                    break;
                case 7:rad2.setIcon(heroes[num].getIcon());
                    break;
                case 8:rad7.setIcon(heroes[num].getIcon());
                    break;
                case 13:rad3.setIcon(heroes[num].getIcon());
                    break;
                case 14:rad8.setIcon(heroes[num].getIcon());
                    break;
                case 15:rad4.setIcon(heroes[num].getIcon());
                    break;
                case 16:rad9.setIcon(heroes[num].getIcon());
                    break;
                case 19:rad10.setIcon(heroes[num].getIcon());
                    break;
                case 20:rad5.setIcon(heroes[num].getIcon());
                    break;
            }
            if (picks.contains(numberOfStep)){
                CHOOSEButton.setText(" PICK " +  heroes[Hero.getNumberOfChosenHero()].getName());
            } else {CHOOSEButton.setText(" BAN " + heroes[Hero.getNumberOfChosenHero()].getName());}
        }
    }
    public class ChooseListener implements ActionListener {
        public void actionPerformed (ActionEvent e){
            if(pickable) {
                progressBar1.setValue(progressBar1.getValue()+5);
                listOfHeroes[Hero.getNumberOfChosenHero()].setEnabled(false);
                if (picks.contains(numberOfStep) ) {
                    pickLabels[numberOfStep-1].setIcon(heroes[Hero.getNumberOfChosenHero()].getIcon());
                    switch (numberOfStep){
                        case 5:picked[5] = heroes[Hero.getNumberOfChosenHero()];
                            break;
                        case 6:picked[0] = heroes[Hero.getNumberOfChosenHero()];
                            break;
                        case 7:picked[1] = heroes[Hero.getNumberOfChosenHero()];
                            break;
                        case 8:picked[6] = heroes[Hero.getNumberOfChosenHero()];
                            break;
                        case 13:picked[2] = heroes[Hero.getNumberOfChosenHero()];
                            break;
                        case 14:picked[7] = heroes[Hero.getNumberOfChosenHero()];
                            break;
                        case 15:picked[3] = heroes[Hero.getNumberOfChosenHero()];
                            break;
                        case 16:picked[8] = heroes[Hero.getNumberOfChosenHero()];
                            break;
                        case 19:picked[9] = heroes[Hero.getNumberOfChosenHero()];
                            break;
                        case 20:picked[4] = heroes[Hero.getNumberOfChosenHero()];
                            break;
                    }
                    listOfHeroes[Hero.getNumberOfChosenHero()].setDisabledIcon(heroes[Hero.getNumberOfChosenHero()].getIconChoose());
                } else {
                    listOfHeroes[Hero.getNumberOfChosenHero()].setDisabledIcon(heroes[Hero.getNumberOfChosenHero()].getIconBanned());
                }
                numberOfStep++;

                if (picks.contains(numberOfStep)){
                    CHOOSEButton.setText(" PICK ");
                } else {CHOOSEButton.setText(" BAN ");}
            }
            pickable = false;
            String message;
            if (direBan.contains(numberOfStep)){
                message = "Dire team ban.";
            } else if (direPick.contains(numberOfStep)) {
                message = "Dire team pick.";
            } else if (radiantBan.contains(numberOfStep)){
                message = "Radiant team ban.";
            } else message = "Radiant team pick.";

            if(numberOfStep<=20) {JOptionPane.showMessageDialog(frame, message);}
            if (numberOfStep > 20){
                finishGame();
               /* String resultOfPick = "Radiant team:";
                for (int i = 0; i < 10; i++) {
                    if (i == 5) resultOfPick = new StringBuilder().append(resultOfPick).append(" Dire team:").toString();
                    resultOfPick = new StringBuilder().append(resultOfPick).append(picked[i]).append(", ").toString();
                }
                System.out.println(resultOfPick);
                JOptionPane.showMessageDialog( frame, resultOfPick);*/
            }

        }
    }

    private void finishGame() {
        StartGame.finish = new FinishForm();
    }

    public void createMenu(){
        JMenuBar menu = new JMenuBar();
        JMenu game = new JMenu("Game");
        JMenuItem newGame = new JMenuItem("New");
        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createNewGame();
            }
        });
        JMenuItem exit = new JMenuItem("Exit");
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        game.add(newGame);
        game.add(exit);
        menu.add(game);
        frame.setJMenuBar(menu);
    }

    public void createNewGame() {
        pickable = false;
        numberOfStep = 1;
        progressBar1.setValue(0);
        for (int i = 0; i < 113; i++) {
            listOfHeroes[i].setIcon(heroes[i].getIcon());
            listOfHeroes[i].setEnabled(true);
        }
        for (int i = 0; i < 20; i++) {
            pickLabels[i].setIcon(new ImageIcon("src/liatri/cm/icons/" + (i+1) + "-ban.png"));
        }
        rad1.setIcon(new ImageIcon("src/liatri/cm/icons/0.png"));
        rad2.setIcon(new ImageIcon("src/liatri/cm/icons/0.png"));
        rad3.setIcon(new ImageIcon("src/liatri/cm/icons/0.png"));
        rad4.setIcon(new ImageIcon("src/liatri/cm/icons/0.png"));
        rad5.setIcon(new ImageIcon("src/liatri/cm/icons/0.png"));
        rad6.setIcon(new ImageIcon("src/liatri/cm/icons/1.png"));
        rad7.setIcon(new ImageIcon("src/liatri/cm/icons/1.png"));
        rad8.setIcon(new ImageIcon("src/liatri/cm/icons/1.png"));
        rad9.setIcon(new ImageIcon("src/liatri/cm/icons/1.png"));
        rad10.setIcon(new ImageIcon("src/liatri/cm/icons/1.png"));
        CHOOSEButton.setText("BAN");
        JOptionPane.showMessageDialog( frame, "Radiant team ban.");
    }

    public void createPickBanArea(){
        pick.setLayout(new GridLayout(11, 2, 5, 5));

        JLabel rad = new JLabel("RADIANT");
        rad.setForeground(Color.lightGray);
        rad.setFont(new Font("Verdana", Font.BOLD, 14));
        rad.setHorizontalAlignment(JLabel.CENTER);

        JLabel dire = new JLabel("DIRE");
        dire.setForeground(Color.lightGray);
        dire.setFont(new Font("Verdana", Font.BOLD, 14));
        dire.setHorizontalAlignment(JLabel.CENTER);

        pick.add(rad);
        pick.add(dire);
        for (int i = 0; i < 20; i++) {
            pickLabels[i] = new JLabel("");
            pickLabels[i].setIcon(new ImageIcon("src/liatri/cm/icons/" + (i+1) + "-ban.png"));
        }

        pick.add(pickLabels[0]);
        pick.add(pickLabels[1]);
        pick.add(pickLabels[2]);
        pick.add(pickLabels[3]);
        pick.add(pickLabels[5]);
        pick.add(pickLabels[4]);
        pick.add(pickLabels[6]);
        pick.add(pickLabels[7]);
        pick.add(pickLabels[9]);
        pick.add(pickLabels[8]);
        pick.add(pickLabels[11]);
        pick.add(pickLabels[10]);
        pick.add(pickLabels[12]);
        pick.add(pickLabels[13]);
        pick.add(pickLabels[14]);
        pick.add(pickLabels[15]);
        pick.add(pickLabels[17]);
        pick.add(pickLabels[16]);
        pick.add(pickLabels[19]);
        pick.add(pickLabels[18]);

        CHOOSEButton.setBorderPainted(false);
        CHOOSEButton.setFocusPainted(false);
        CHOOSEButton.addActionListener(new ChooseListener());
    }
    public class HeroButton extends JButton {
        private int pos;

        public int getPos() {
            return pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }
    }
}
